
import nltk
from nltk.corpus import stopwords

from nltk.tokenize import sent_tokenize, word_tokenize
EXAMPLE_TEXT = "Hello Mr. Smith, how are you doing today? The weather is great, and Python is awesome. The sky is pinkish-blue. You shouldn't eat cardboard."
arr =[]
arr = word_tokenize(EXAMPLE_TEXT)
stop_words = set(stopwords.words('english'))
print '**********************************************************************************'
print arr
print '**********************************************************************************'
print stop_words
print '**********************************************************************************'
