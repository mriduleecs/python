import json
import requests
import subprocess
import sys
import os
from gtts import gTTS

def main(argv):
    locale = argv
    apiurl = 'http://api.openweathermap.org/data/2.5/weather?q=XXX&appid=248d4a79c62170e0596b545921cc7be4'
    url = apiurl.replace("XXX",locale)
    jsondata = requests.get(url).json()

#    print '****************************************************************\n'
#    print jsondata
#    print '\n****************************************************************\n'

    jsonclouds = jsondata['clouds']['all']
#    print jsonclouds
    jsoncloudsdesc = jsondata['weather'][0]['description']#['description']
#    print jsoncloudsdesc
    jsonmain = jsondata['main']
    jsonwind = jsondata['wind']
    temperature = jsondata['main']['temp']
#    clouds = jsondata['main']
    humidity = jsondata['main']['humidity']
    print '****************************************************************\n'
    speech = 'Temperature in ' + locale + ' is '+ str(temperature-273.15) + ' degrees.'\
    +' Humidity in ' + locale + ' is ' +str(humidity) + '%.'\
    +' Clouds are ' + str(jsonclouds) +"% in the sky"\
    +'. Weather is ' + jsoncloudsdesc +'.'
    print speech
    tts = gTTS(speech , 'en')
    tts.save('speech.mp4')
    os.system('mpg321 speech.mp4')
    print '\n****************************************************************\n'




if __name__ == "__main__":
   main(sys.argv[1])
