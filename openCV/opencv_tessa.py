import cv2
import pytesseract
from PIL import Image
import sys

def tess(path):
	img = cv2.imread(path)
	img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	#adaptivegaussian = cv2.adaptiveThreshold(img2, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)
	#adaptivegaussian2 = cv2.adaptiveThreshold(img2, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
	img2 = cv2.adaptiveThreshold(img1, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)
	cv2.imwrite('filtered.jpg',img2)
	print "Out of the loop"
	result = pytesseract.image_to_string(Image.open('filtered.jpg'))
	print result
	cv2.imshow(path,img2)
	cv2.waitKey(0)
	cv2.destroyAllWindows()

print sys.argv[1]
tess(sys.argv[1])
