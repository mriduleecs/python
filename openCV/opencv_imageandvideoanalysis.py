import numpy as np
import cv2

img1 = cv2.imread('3D-Matplotlib.png',cv2.IMREAD_COLOR)
img2 = cv2.imread('mainsvmimage.png',cv2.IMREAD_COLOR)
img3 = cv2.imread('newspapers.jpg',cv2.IMREAD_COLOR)
# img[260:700, 170:670] = [255, 0, 0]
# watch_face = img1[260:700, 170:670]
#add=cv2.addWeighted(img1, 0.6, img2, 0.4, 0)
img3gray = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
#img3color = cv2.cvtColor(img3, cv2.COLOR_BGR2BGRA)

#NORMAL THRESHOLDING
retval, mask = cv2.threshold(img3, 10, 255, cv2.THRESH_BINARY)

#ADAPTIVE GAUSIAN THRESHOLDING
#retval, mask2 = cv2.adaptiveThreshold(img3, 10, 255, cv2.THRESH_BINARY)
adaptivegaussian = cv2.adaptiveThreshold(img3gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 115, 1)
adaptivegaussian2 = cv2.adaptiveThreshold(img3gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

#ADAPTIVE OTSU THRESHOLDING
retval2, otsu = cv2.threshold(img3gray, 125, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)



cv2.imshow('image', mask)
cv2.imshow('image2', img3)
cv2.imshow('adaptive gaussian', adaptivegaussian)
cv2.imshow('adaptive gaussian2', adaptivegaussian2)
#cv2.imshow('otsu', otsu)

cv2.waitKey(0)
cv2.destroyAllWindows()
