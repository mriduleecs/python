import cv2
import numpy as np

facecascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eyecascade = cv2.CascadeClassifier("haarcascade_eye.xml")
#eyeglassescascade = cv2.CascadeClassifier("haarcascade_eye_tree_eyeglasses.xml")

cap = cv2.VideoCapture(0)

while True:
	__, img = cap.read()
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	faces = facecascade.detectMultiScale(gray)
	for(x, y, w, h) in faces:
		cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
		roi_gray = gray[y:y + h, x:x + w]
		roi_color = img[y:y + h, x:x + w]
		eyes = eyecascade.detectMultiScale(roi_gray)
		for (ex, ey, ew, eh) in eyes:
			cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)


	cv2.imshow('img', img)
	k = cv2.waitKey(30) &  0xFF
	if k == 27:
		break

cap.release()
cv2.destroyAllWindows()
