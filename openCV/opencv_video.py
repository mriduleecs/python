import numpy as np
import cv2

cap=cv2.VideoCapture(0)
fourcc=cv2.VideoWriter_fourcc(*'XVID')
out=cv2.VideoWriter('mywebcam0.avi', fourcc,10.0,(640,480))

while True:
    ret, frame = cap.read()
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    out.write(frame)
    cv2.imshow('v_frame',frame)
    cv2.imshow('v_frame_gray',gray)
    if cv2.waitKey(10) & 0XFF == ord('q'):
        break

cap.release()
out.release()
cv2.destroyAllWindows()
    
