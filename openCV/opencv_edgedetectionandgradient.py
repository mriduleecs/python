import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while True:
	_, frame = cap.read()
	frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	laplacian = cv2.Laplacian(frame, cv2.CV_64F)
	sobelx = cv2.Sobel(frame, cv2.CV_64F, 1, 0, ksize=5)
	sobely = cv2.Sobel(frame, cv2.CV_64F, 0, 1, ksize=5)
	edges =  cv2.Canny(frame, 50, 50)

	cv2.imshow('frame', frame)
	cv2.imshow('laplacian', laplacian)
	#cv2.imshow('sobelx', sobelx)
	cv2.imshow('canny', edges)

	if (cv2.waitKey(10) & 0xFF) == ord('q'):
		break

cv2.destroyAllWindows()
cap.release()
