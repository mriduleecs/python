import numpy as np
import cv2

img = cv2.imread('watch.jpg',cv2.IMREAD_COLOR)
cv2.line(img, (0,0) , (150,150) , (0,0,255) , 20)
cv2.rectangle( img, (15,25) , (600,600) , (0,255,0) , -10 )
cv2.circle(img, (300,300) ,  60 , (255,0,0) , 10 )

pts = np.array([[10, 5], [20, 30], [40, 50], [60, 60], [60, 30], [40, 5], [10, 5]], np.int32)
cv2.polylines(img, [pts], False, (255, 255, 0), 5)
font = cv2.FONT_HERSHEY_COMPLEX
cv2.putText(img, 'Hey Mridul Whats up!!', (200, 500),  font, 1, (0, 255, 255), 2, cv2.LINE_AA)

cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()
