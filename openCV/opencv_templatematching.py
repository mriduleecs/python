import numpy as np
import cv2

img_bgr = cv2.imread('kapil.png')
img_gray = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2GRAY)

template = cv2.imread('kapillefteye.png')
templategray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

template2 = cv2.imread('kapilrighteye.png')
templategray2 = cv2.cvtColor(template2, cv2.COLOR_BGR2GRAY)

cv2.imshow('templategray2', templategray2)

w, h = templategray.shape[::-1]

res = cv2.matchTemplate(img_gray, templategray, cv2.TM_CCOEFF_NORMED)
threshold = 0.9
loc = np.where(res >= threshold)

res2 = cv2.matchTemplate(img_gray, templategray2, cv2.TM_CCOEFF_NORMED)
#threshold = 0.9
loc2 = np.where(res2 >= threshold)

for pt in zip(*loc[::-1]):
	cv2.rectangle(img_bgr, pt, (pt[0]+w, pt[1]+h), (0, 0, 255), 1)

for pt in zip(*loc2[::-1]):
	cv2.rectangle(img_bgr, pt, (pt[0]+w, pt[1]+h), (0, 255, 255), 1)

cv2.imshow('img_bgr',img_bgr)
cv2.waitKey(0)
cv2.destroyAllWindows()
