import numpy as np
import cv2

cap = cv2.VideoCapture(0)
red = np.uint8([[[0, 0, 255]]])
#blue = np.uint8([[[0, 0, 0]]])
hsv_red = cv2.cvtColor(red,cv2.COLOR_BGR2HSV)
print(hsv_red)
_, frame = cap.read()

hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

#print(hsv)

while True:
	_, frame = cap.read()
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	lowerred = np.array([-10, 100, 0])
	upperred = np.array([10, 255, 255])

	lowerblue = np.array([110, 100, 0])
	upperblue = np.array([130, 255, 255])

	maskerred = cv2.inRange(hsv, lowerred, upperred)
	red = cv2.bitwise_and(frame, frame, mask=maskerred)

	maskerblue = cv2.inRange(hsv, lowerblue, upperblue)
	blue = cv2.bitwise_and(frame, frame, mask=maskerblue)
	#print(hsv)


	#cv2.imshow('frame', frame)
	#cv2.imshow('masker', masker)
	cv2.imshow('RED FILTER', red)
	cv2.imshow('BLUE FILTER', blue)

	if (cv2.waitKey(10) & 0xFF) == ord('q'):
		break

cv2.destroyAllWindows()
cap.release()
