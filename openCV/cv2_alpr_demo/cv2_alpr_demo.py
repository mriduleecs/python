import numpy as np
import cv2
from openalpr import Alpr
import time
import preprocess
import math
import random
from skimage import data

alpr = Alpr("us", "/home/mridul/cv2_alpr_demo/alpr/config/us.conf", "/home/mridul/cv2_alpr_demo/alpr")
if not alpr.is_loaded():
    print("Error loading OpenALPR")
    sys.exit(1)

alpr.set_top_n(1)
alpr.set_default_region("us")
cap = cv2.VideoCapture("/home/mridul/cv2_alpr_demo/lic.avi")
framecount = 0
kernel = np.array([[-1,-1,-1], [-1,8,-1], [-1,-1,-1]])
while(True):
    ret, frame = cap.read()
    #frame = cv2.filter2D(frame, -1, kernel)
    #thresh1 = frame#cv2.adaptiveThreshold(frame, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY , 31, 2)
    framecount += 1
    #cv2.Laplacian(frame, cv2.CV_64F)#frame)
    #if(framecount % 1 == 0):
    #filteredframe = thresh1#cv2.cvtColor(thresh1, cv2.COLOR_BGR2GRAY)#cv2.filter2D(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY), -1, kernel)
    #height, width, layers  = filteredframe.shape
    #res = filteredframe#cv2.resize(filteredframe,(2*width, 2*height), interpolation = cv2.INTER_CUBIC)
    if(framecount % 10 == 0):
        cv2.imshow('frame', frame)
        cv2.imwrite('img.jpg',frame);
        results = alpr.recognize_file('img.jpg')
        if cv2.waitKey(10) & 0xFF == ord('q'):
            break
        i = 0
        for plate in results['results']:
            i += 1
            for candidate in plate['candidates']:
                #if candidate['confidence'] > 10:
                print("Plate #%d" % i)
                print("   %12s %12s" % ("Plate", "Confidence"))
                prefix = "-"
                print("  %s %12s%12f" % (prefix, candidate['plate'], candidate['confidence']))


# When everything done, release the capture
cap.release()
alpr.unload()
cv2.destroyAllWindows()



'''
#height, width  = filteredframe.shape
#cv2.imwrite("img.jpg", filteredframe)
plates = plate_cascade.detectMultiScale(filteredframe, 1.3, 5)
for (x,y,w,h) in plates:
    cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
    roi_gray = filteredframe[y:y+h, x:x+w]
    roi_color = frame[y:y+h, x:x+w]
'''
