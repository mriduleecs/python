import sys
import cv2
import numpy as np
from cv2 import cv


cap = cv2.VideoCapture(0)
cap.set(cv.CV_CAP_PROP_FRAME_WIDTH,640)
cap.set(cv.CV_CAP_PROP_FRAME_HEIGHT,480)
cap.set(cv.CV_CAP_PROP_CONTRAST,0.9)
cap.set(cv.CV_CAP_PROP_BRIGHTNESS,0.5)
cap.set(cv.CV_CAP_PROP_GAIN,.1)
cap.set(cv.CV_CAP_PROP_SATURATION,1)
exitflag = 0

fourcc = cv.CV_FOURCC(*'XVID')
out = cv2.VideoWriter('videocapture.avi',fourcc, 20.0, (640,480))

while(cap.isOpened()):
    ret, frame = cap.read()
    #frame = cv2.flip(frame, 0)
    cv2.imshow("FRAME", frame)
    out.write(frame)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        exitflag = 1
        break

if exitflag == 1:
    cap.release()
    out.release()
    cv2.destroyAllWindows()
    exit(0)
print "Camera not found \n Try reconnecting or alter configurations of the frame using cap.set()\n"
