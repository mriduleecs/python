import sys
import numpy as np
import cv2
import time


cap = cv2.VideoCapture(0)
cap.set(3,1920)
cap.set(4,1080)

while(True):
    if(cap.isOpened()):
        #Capture frame by frame
        ret, frame = cap.read()
        #Our operation on the frame code here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #Display resulting frame
        cv2.imshow("FRAME", frame)
        cv2.imshow("GRAY", gray)
        cv2.imwrite("camcapture.jpg",gray)
    else:
        print "No Camera detected on this machine try checking camera connection \nor try \"cap.set(3,1920)\" and \"cap.set(4,1080)\"...\n"
        break
        #cap.open()
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
