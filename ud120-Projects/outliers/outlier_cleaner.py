#!/usr/bin/python


def outlierCleaner(predictions, ages, net_worths):
    """
        Clean away the 10% of points that have the largest
        residual errors (difference between the prediction
        and the actual net worth).

        Return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error).
    """
    
    cleaned_data = []
    largest = {}
    error = []
    sorted_list  = []
    perc = 10
    outfit = []
    errorlist = []
    ### your code goes here
    error = predictions - net_worths            ##first step : we created a list of errors by subtraction
    for i in range(len(error)):
        largest[abs(error[i][0])] = i           ##second step : we created a dict named largest with error and their indexes in dataset
        errorlist.append(error[i][0])           ##              we created a list names errorlist with errors

    perc = (perc*len(error)/100)
    largest = sorted(largest.items(), reverse=True)[0:perc]    #we sorted dictionary with their absolute error values and sliced its 10%
    print largest
    i = 0
    for key, items in largest:
        outfit.append(largest[:][i][1])                        #we created a list named outfit with the indexes of outfits with largest absolute error
        i+=1


    for i in range(len(ages)):
        if i not in outfit:
            cleaned_data.append((ages[i], net_worths[i], errorlist[i]))# we created a list of tuples with ages, networth, error(not absolute) with indexes that are not in outfit/largest errors

    return cleaned_data


