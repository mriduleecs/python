#!/usr/bin/python

import pickle
import sys
import matplotlib.pyplot
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit


### read in data dictionary, convert to numpy array
data_dict = pickle.load( open("../final_project/final_project_dataset.pkl", "r") )
features = ["salary", "bonus"]
data = featureFormat(data_dict, features)
target, features = targetFeatureSplit(data)

from sklearn.model_selection import train_test_split
feature_train, feature_test, target_train, target_test = train_test_split(features, target, test_size=0.5, random_state=42)
#print data_dict
### your code below

from sklearn.linear_model import LinearRegression
reg = LinearRegression()
reg.fit(feature_train, target_train)

sortlist = {}
for i in range(len(data)):
    error = abs((data[i][0] - reg.predict(data[i][0])))
    sortlist[error[0]] = i

sortlist = sorted(sortlist.items(), reverse =True)
print 'biggest outfitter index in data -> ', sortlist[0][1]
print data[sortlist[1][1]], data[sortlist[2][1]]
#print reg.predict(feature_test)
print reg.score(feature_test, target_test)


for point in data:
    salary = point[0]
    bonus = point[1]
    matplotlib.pyplot.scatter( salary, bonus )
    matplotlib.pyplot.plot(feature_train, reg.predict(feature_train), color="r")
matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()
#print data_dict
