#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle
import sys
sys.path.append("../final_project/")
from poi_email_addresses import poiEmails


enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))
#print len(enron_data)
#print enron_data['TAYLOR MITCHELL S']#['poi']
count = 0.0
prev = 0
temp = 0
dict = {}
max = 0
name = 'q'
feild = 'deferral_payments'
total = 0.0
    print enron_data
for i in enron_data:
    '''if("FASTOW" in i):
        print enron_data[i]
    print i, "->",  enron_data[i][feild]
'''
    #if(enron_data[i][feild] != 'NaN' and not('TOTAL' in i)):
    temp = enron_data[i][feild]
    if(enron_data[i][feild] == 'NaN'):
        count += 1
    total+=1

#print "\nMAX", name, '->', max
#print dict
print '\n\n', (dict.items())

print '\n\nTotal = ', count, total
perc = 0.12
perc = count/(total-1)
print perc
#el = poiEmails()
#print len(el)
#print max

'''
count = 0
f = open("../final_project/poi_names.txt", 'r')
rd = f.read().splitlines()
for i in range(len(rd)):
    if('(' in rd[i]):
        count+=1

print count
'''