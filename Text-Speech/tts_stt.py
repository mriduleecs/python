import os
import winsound
import wikipedia as wiki
from gtts import gTTS
import gtts
from subprocess import call
import speech_recognition as sr

query = "california"


for index, name in enumerate(sr.Microphone.list_microphone_names()):
    print("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))

r = sr.Recognizer()

print(" Adjusting background noise ")
call(['espeak ', '-v','us2\\us2' , '-g', '1',"Adjusting background noise"])

with sr.Microphone() as source:
    r.adjust_for_ambient_noise(source)

with sr.Microphone() as source:
    call(['espeak ', '-v','us2\\us2' , '-g', '1',"Please say something and i will try to understand. "])
    print(" Say something : ")
    audio = r.listen(source)
    print(" OK ")

try:
    # for testing purposes, we're just using the default API key
    # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
    # instead of `r.recognize_google(audio)`
    query = r.recognize_google(audio)
    print("Google Speech Recognition thinks you said " + query)
except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))



#query = "schizophrenia"
replyfromwiki = wiki.summary(query)
tts = gTTS(text=replyfromwiki, lang='en')
#tts.save("wikisummary.wav")
print(replyfromwiki)
call(['espeak ', '-v','us2\\us2' , '-g', '2',replyfromwiki])

#espeak_location = ".\\espeak.exe "
#os.system(espeak_location+"hello world")
