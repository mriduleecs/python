from flask import Flask
from flask_ask import Ask, statement, question, session
import json		#import data from reddit
import requests		#requests to web page
import time
import unidecode
from werkzeug.exceptions import abort


app = Flask(__name__)
ask = Ask(app, "/reddit_reader")

def get_headlines():
    user_pass = {"user" : "" , "passwd" : "" , "api_type" : "json"}
    sess = requests.session()
    sess.headers.update({"User-Agent":"This is a alexa testing "})
    sess.post('https://www.reddit.com/api/login', data = user_pass)
    time.sleep(1)
    url = 'https://reddit.com/r/worldnews/.json'
    html = sess.get(url)
    jsonloads = json.loads(html.content.decode('utf-8'))
    titles = []
    for listings in jsonloads['data']['children']:
        titles.append(unidecode.unidecode(listings['data']['title']))
    titles = '... '.join([i for i in titles])
    return titles

titles = get_headlines()
print titles

@app.route('/')
def home():
    return "Hi there how are you doing"

@ask.launch
def start_skill():
    Welcome_message = "Hello there, would you like the news?"
    return question(Welcome_message)

@ask.intent("YESintent")
def share_headines():
    headlines = get_headlines()
    headline_message = "The current world news headlines are {}" .format(headlines)
    return statement(headline_message)

@ask.intent("NOintent")
def no_intent():
    byetext = "I am not sure why you asked me to run then,  but okay.... bye..."
    return statement(byetext)






if __name__ == '__main__':
    app.run()
