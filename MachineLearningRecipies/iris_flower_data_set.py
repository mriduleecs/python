'''
    GOALS
    1. Import dataset from iris flower data dataset
    2. Train a classifier
    3. Predict label for new flower

'''
#dataset import
from sklearn.datasets import load_iris
import numpy as np
from sklearn import tree

iris = load_iris()
for i in range(len(iris.target)):
    print "Example %d: label %s: features %s" %(i, iris.target[i], iris.data[i])

#deleting some data from the datasets
test_idx = [0,50,100]
train_target = np.delete(iris.target, test_idx) #delete 0th 100th and 150th data from iris.target(label)
train_data = np.delete(iris.data, test_idx, axis = 0) #delete 0th 100th and 150th data from iris.data(features)

#testing data
test_target = iris.target[test_idx]
test_data = iris.data[test_idx]

#Train a classifier
from sklearn import tree

clf = tree.DecisionTreeClassifier()
clf.fit(train_data, train_target)

#Predict Label for New flowers


#Visualise the Tree
import graphviz
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render("iris")

dot_data = tree.export_graphviz(clf, out_file=None,
                         feature_names=iris.feature_names,
                         class_names=iris.target_names,
                         filled=True, rounded=True,
                         special_characters=True)
graph = graphviz.Source(dot_data)
#graph.show();
