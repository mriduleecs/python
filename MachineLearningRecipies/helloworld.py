from sklearn import tree
#features are inputs to the classifiers
#0 stand for smooth and 1 stand for bumpy
features = [[140, 0], [130, 0], [150, 1], [170, 1]]
#labels are the outputs
labels = ["apple", "apple", "orange", "orange"]

#classifiers
clf = tree.DecisionTreeClassifier()
#find patterns in data
clf = clf.fit(features, labels)
print clf.predict([[150, 1]])
